package ru.nikita.javaTasksForCTF.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.nikita.javaTasksForCTF.domain.User;
import ru.nikita.javaTasksForCTF.domain.UserDao;
import ru.nikita.javaTasksForCTF.services.UserService;
import ru.nikita.javaTasksForCTF.validators.UserValidator;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;

@Controller
@Slf4j
public class TaskAdmin1Controller {
    private UserService userService;
    private UserValidator userValidator;
    private final String FLAG = "46f3d303-dcec-4553-8cd2-c6992cca03d7";

    public TaskAdmin1Controller(UserService userService, UserValidator userValidator) {
        this.userService = userService;
        this.userValidator = userValidator;
    }

    @RequestMapping(value = "/taskAdmin1/home", method = RequestMethod.GET)
    public String getHome(@CookieValue(value = "hash", defaultValue = "default") String hash, Model model){
        if(hash.equals("default")){
            model.addAttribute("authorized", false);
        }else{
            User user = userService.findByCookie(hash);
            if(user != null){
                model.addAttribute("authorized", true);
                model.addAttribute("user", user);
                return "/taskAdmin1/home";
            }else{
                model.addAttribute("authorized", false);
            }
        }
        return "redirect:/taskAdmin1/login";
    }

    @RequestMapping(value = "/taskAdmin1", method = RequestMethod.GET)
    public String get(@CookieValue(value = "hash", defaultValue = "default") String hash, Model model){
        return "redirect:/taskAdmin1/home";
    }

    @RequestMapping(value = "/taskAdmin1/exit", method = RequestMethod.GET)
    public String getExit(HttpServletResponse httpServletResponse){
        httpServletResponse.addCookie(new Cookie("hash", ""));
        return "redirect:/taskAdmin1/login";
    }

    @RequestMapping(value = "/taskAdmin1/secret", method = RequestMethod.GET)
    public String getSecret(@CookieValue(value = "hash", defaultValue = "default") String hash, Model model){
        if(hash.equals("default")){
            model.addAttribute("authorized", false);
        }else{
            User user = userService.findByCookie(hash);
            if(user != null){
                model.addAttribute("authorized", true);
                model.addAttribute("user", user);
                if(user.getLogin().equals("admin")){
                    model.addAttribute("admin", true);
                    model.addAttribute("secret", FLAG);
                }else{
                    model.addAttribute("admin", false);
                }
                return "/taskAdmin1/secret";
            }else{
                model.addAttribute("authorized", false);
            }
        }
        return "redirect:/taskAdmin1/login";
    }


    @RequestMapping(value = "/taskAdmin1/login", method = RequestMethod.GET)
    public String getLogin(@CookieValue(value = "hash", defaultValue = "default") String hash, Model model){
        if(hash.equals("default")){
            model.addAttribute("authorized", false);
        }else{
            if(userService.findByCookie(hash) != null){
                model.addAttribute("authorized", true);
            }else{
                model.addAttribute("authorized", false);
            }
        }
        model.addAttribute("userDao", new UserDao());
        return "/taskAdmin1/login";
    }

    @RequestMapping(value = "/taskAdmin1/login", method = RequestMethod.POST)
    public String postLogin(@Valid @ModelAttribute("userDao") UserDao userDao,
                                   @CookieValue(value = "hash", defaultValue = "default") String hash, Model model,
                                   HttpServletResponse httpServletResponse) throws Exception {
        if(hash.equals("default")){
            model.addAttribute("authorized", false);
        }else{
            if(userService.findByCookie(hash) != null){
                model.addAttribute("authorized", true);
            }else{
                model.addAttribute("authorized", false);
            }
        }
        userValidator.trimFields(userDao);
        User user = userService.findByLoginAndPassword(userDao.getLogin(), userDao.getPassword());
        if(user == null){
            model.addAttribute("error", true);
            model.addAttribute("userDao", userDao);
            return "/taskAdmin1/login";
        }else{
            httpServletResponse.addCookie(new Cookie("hash", user.getCookie()));
            return "redirect:/taskAdmin1/home";
        }
    }

    @RequestMapping(value = "/taskAdmin1/registration", method = RequestMethod.GET)
    public String getRegistration(@CookieValue(value = "hash", defaultValue = "default") String hash, Model model){
        if(hash.equals("default")){
            model.addAttribute("authorized", false);
        }else{
            if(userService.findByCookie(hash) != null){
                model.addAttribute("authorized", true);
            }else{
                model.addAttribute("authorized", false);
            }
        }
        model.addAttribute("userDao", new UserDao());
        return "/taskAdmin1/registration";
    }

    @RequestMapping(value = "/taskAdmin1/registration", method = RequestMethod.POST)
    public String postRegistration(@Valid @ModelAttribute("userDao") UserDao userDao, BindingResult result,
                                   @CookieValue(value = "hash", defaultValue = "default") String hash, Model model,
                                   HttpServletResponse httpServletResponse) throws Exception {
        if(hash.equals("default")){
            model.addAttribute("authorized", false);
        }else{
            if(userService.findByCookie(hash) != null){
                model.addAttribute("authorized", true);
            }else{
                model.addAttribute("authorized", false);
            }
        }
        userValidator.trimFields(userDao);
        userValidator.validate(userDao, result);
        if(result.hasErrors()){
            model.addAttribute("userDao", userDao);
            return "/taskAdmin1/registration";
        }
        User user = userService.create(userDao);
        httpServletResponse.addCookie(new Cookie("hash", user.getCookie()));
        return "redirect:/taskAdmin1/home";
    }

    @ExceptionHandler(NumberFormatException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public void handleException3(NumberFormatException ex, HttpServletResponse response) throws IOException
    {

    }

}
