package ru.nikita.javaTasksForCTF.controllers;

import com.google.common.hash.Hashing;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.nikita.javaTasksForCTF.domain.CryptoData;
import ru.nikita.javaTasksForCTF.services.CryptoDataService;

import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.*;

@Controller
@Slf4j
public class TaskCrypto1Controller {

    private CryptoDataService cryptoDataService;
    private final String MESSAGE = "very important information";
    private final String FLAG = "8e93c634-1f07-4cda-b9cb-577fe4563f02";

    public TaskCrypto1Controller(CryptoDataService cryptoDataService) {
        this.cryptoDataService = cryptoDataService;
    }

    private boolean verify(BigInteger signature, BigInteger e, BigInteger n){
        String decrypt = signature.modPow(e, n.abs()).toString(16);
        String padHex = PKCS1_pad(Hashing.sha1().hashString(MESSAGE, StandardCharsets.UTF_8).toString());
        return decrypt.equals(padHex);
    }

    private String PKCS1_pad(String data){
        String asn1 = "003021300906052b0e03021a05000414";
        String ans = asn1 + data;
        int len = ans.length();
        StringBuilder builder = new StringBuilder("1");
        for(int i = 0; i < (1024/4-len-4); i++){
            builder.append('f');
        }
        String padding = builder.toString();
        String result = padding+ans;
        return result;
    }

    private BigInteger getSignature() throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        BigInteger s;
            KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
            keyGen.initialize(1024, new SecureRandom());
            KeyPair keyPair = keyGen.generateKeyPair();


            Signature signer = Signature.getInstance("SHA1withRSA");
            signer.initSign(keyPair.getPrivate());
            signer.update(MESSAGE.getBytes());
            byte[] signature = signer.sign();

            s = new BigInteger(signature);
        return s;
    }


    @RequestMapping(value = "/taskCrypto1", method = RequestMethod.GET)
    public String getTask(Model model) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, SignatureException {
        BigInteger bigInteger = getSignature();
        String signature = bigInteger.toString();
        long id = cryptoDataService.create(bigInteger.toString());
        model.addAttribute("id", id);
        model.addAttribute("message", MESSAGE);
        model.addAttribute("sign", signature);
        return "/taskCrypto1/home";

    }

    @RequestMapping(value = "/taskCrypto1", method = RequestMethod.POST)
    public String getTask(@RequestParam("id") String idStr,
                          @RequestParam("e") String e,
                          @RequestParam("n") String n,
                          Model model) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, SignatureException {
            long id = Long.valueOf(idStr);
            CryptoData cryptoData = cryptoDataService.getById(id);
            boolean v;
            v = verify(new BigInteger(cryptoData.getSignature()), new BigInteger(e), new BigInteger(n));
            cryptoDataService.delete(id);
            if (v) {
                model.addAttribute("flag", FLAG);
                return "/taskCrypto1/flag";
            } else {
                model.addAttribute("fail", true);
            }
            BigInteger bigInteger = getSignature();
            String signature = bigInteger.toString(16);
            id = cryptoDataService.create(bigInteger.toString(16));
            model.addAttribute("id", id);
            model.addAttribute("message", MESSAGE);
            model.addAttribute("sign", signature);
            return "/taskCrypto1/home";

    }

    @RequestMapping(value = "/taskCrypto1/code", method = RequestMethod.GET)
    public void getFile(HttpServletResponse response){
        try{
            InputStream is = new FileInputStream(new File("./code.java"));
            org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
            response.flushBuffer();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            log.info("Error writing file to output stream.");
            throw new RuntimeException("IOError writing file to output stream");
        }
    }

}
