package ru.nikita.javaTasksForCTF.services;

import com.google.common.hash.Hashing;
import org.springframework.stereotype.Service;
import ru.nikita.javaTasksForCTF.domain.User;
import ru.nikita.javaTasksForCTF.domain.UserDao;
import ru.nikita.javaTasksForCTF.repositories.UserRepository;

import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


//
    @Override
    public User create(UserDao userDao) {
        Optional<User> userOptional = userRepository.findByLoginEquals(userDao.getLogin());
        if (userOptional.isPresent()){
            throw new RuntimeException("User with this login already exist");
        }
        User user = new User();
        user.setLogin(userDao.getLogin());
        user.setSalt(UUID.randomUUID().toString());
        String sha256hex = Hashing.sha256()
                .hashString(userDao.getPassword()+user.getSalt(), StandardCharsets.UTF_8)
                .toString();
        user.setPasswordHash(sha256hex);
        String loginHash = Hashing.md5().hashString(userDao.getLogin(), StandardCharsets.UTF_8).toString();
        String successHash = Hashing.md5().hashString("success", StandardCharsets.UTF_8).toString();
        user.setCookie(loginHash+successHash);
        return userRepository.save(user);
    }

    @Override
    public User findByLoginAndPassword(String login, String password) {
        Optional<User> userOptional = userRepository.findByLoginEquals(login);
        if(!userOptional.isPresent()){return null;}
        User user = userOptional.get();
        String sha256hex = Hashing.sha256()
                .hashString(password+user.getSalt(), StandardCharsets.UTF_8)
                .toString();
        userOptional = userRepository.findByLoginEqualsAndPasswordHashEquals(login, sha256hex);
        return userOptional.orElse(null);
    }

    @Override
    public User findByLogin(String login) {
        Optional<User> userOptional = userRepository.findByLoginEquals(login);
        return userOptional.orElse(null);
    }

    @Override
    public User findByCookie(String cookie) {
        Optional<User> userOptional = userRepository.findByCookieEquals(cookie);
        return userOptional.orElse(null);
    }


}
