package ru.nikita.javaTasksForCTF.services;

import ru.nikita.javaTasksForCTF.domain.User;
import ru.nikita.javaTasksForCTF.domain.UserDao;

public interface UserService {
    User create(UserDao userDao) throws Exception;
    User findByLoginAndPassword(String login, String password);
    User findByLogin(String login);
    User findByCookie(String cookie);

}
