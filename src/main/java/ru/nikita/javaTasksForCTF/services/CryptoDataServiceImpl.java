package ru.nikita.javaTasksForCTF.services;

import org.springframework.stereotype.Service;
import ru.nikita.javaTasksForCTF.domain.CryptoData;
import ru.nikita.javaTasksForCTF.repositories.CryptoDataRepository;

import java.util.Optional;

@Service
public class CryptoDataServiceImpl implements CryptoDataService{
    private CryptoDataRepository repository;

    public CryptoDataServiceImpl(CryptoDataRepository repository) {
        this.repository = repository;
    }

    @Override
    public Long create(String signature) {
        CryptoData cryptoData = new CryptoData();
        cryptoData.setSignature(signature);
        cryptoData = repository.save(cryptoData);
        return cryptoData.getId();
    }

    @Override
    public CryptoData getById(Long id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }
}
