package ru.nikita.javaTasksForCTF.services;

import ru.nikita.javaTasksForCTF.domain.CryptoData;

public interface CryptoDataService {

    Long create(String signature);
    CryptoData getById(Long id);
    void delete(Long id);
}
