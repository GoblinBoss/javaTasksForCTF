package ru.nikita.javaTasksForCTF;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaTasksForCTFApplication {

	public static void main(String[] args) {

		SpringApplication.run(JavaTasksForCTFApplication.class, args);
	}
}
