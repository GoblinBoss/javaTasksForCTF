package ru.nikita.javaTasksForCTF.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ru.nikita.javaTasksForCTF.domain.User;

import java.util.Optional;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long> {

    Optional<User> findByLoginEquals(String login);
    Optional<User> findByLoginEqualsAndPasswordHashEquals(String login, String passwordHash);
    Optional<User> findByCookieEquals(String cookie);

}
