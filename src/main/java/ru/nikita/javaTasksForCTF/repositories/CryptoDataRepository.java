package ru.nikita.javaTasksForCTF.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ru.nikita.javaTasksForCTF.domain.CryptoData;
import ru.nikita.javaTasksForCTF.domain.User;

import java.util.Optional;

@Repository
public interface CryptoDataRepository extends PagingAndSortingRepository<CryptoData, Long> {

    Optional<CryptoData> findById(Long id);


}
