package ru.nikita.javaTasksForCTF.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDao {
    private String login;
    private String password;
}
