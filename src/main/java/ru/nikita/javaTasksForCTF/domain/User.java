package ru.nikita.javaTasksForCTF.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, unique = true)
    @NotNull
    private String login;
    @Column(nullable = false)
    @NotNull
    private String passwordHash;
    @Column(nullable = false)
    @NotNull
    private String salt;
    @Column(nullable = false)
    @NotNull
    private  String cookie;
}

