package ru.nikita.javaTasksForCTF.bootstrap;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import ru.nikita.javaTasksForCTF.domain.UserDao;
import ru.nikita.javaTasksForCTF.services.UserService;

import java.util.UUID;


@Component
@Slf4j
public class Bootstrap implements ApplicationListener<ContextRefreshedEvent> {
    private UserService userService;

    public Bootstrap(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        try {
            if(userService.findByLogin("admin") == null){
                UserDao userDao = new UserDao();
                userDao.setLogin("admin");
                userDao.setPassword(UUID.randomUUID().toString());
                userService.create(userDao);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addEntities() throws Exception {

    }
}
