package ru.nikita.javaTasksForCTF.validators;

import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ru.nikita.javaTasksForCTF.domain.User;
import ru.nikita.javaTasksForCTF.domain.UserDao;
import ru.nikita.javaTasksForCTF.services.UserService;

import java.util.regex.Pattern;

@Component
public class UserValidator implements Validator{
    private UserService userService;

    public UserValidator(UserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Override
    public void validate(@Nullable Object o, Errors errors) {
        UserDao user = (UserDao) o;
        Pattern pattern = Pattern.compile("^[a-zA-Z0-9_]*$");

        if((user.getLogin().length() < 5 || user.getLogin().length() > 32)){
            errors.rejectValue("login", "error.login", "размер логина должен быть между 5 и 32 символами");
        }
        if(user.getLogin().length() >= 5 && user.getLogin().length() <= 32 && !pattern.matcher(user.getLogin()).matches()){
            errors.rejectValue("login","error.login", "логин может содержать только символы \"a-Z A-Z 0-9 _\"");
        }
        User existUser = userService.findByLogin(user.getLogin());
        if(existUser != null){
            errors.rejectValue("login","error.login", "пользователь с таким логином уже зарегистрирован");
        }
        if((user.getPassword().length() <6 || user.getPassword().length() > 32)){
            errors.rejectValue("password", "error.password", "размер пароля должен быть между 6 и 32 символами");
        }
        if(user.getPassword().length() <= 32 && !pattern.matcher(user.getPassword()).matches()){
            errors.rejectValue("password","error.password", "пароль может содержать только символы \"a-Z A-Z 0-9 _\"");
        }
    }

    public void trimFields(UserDao userDao) {
        userDao.setLogin(userDao.getLogin().trim());
        userDao.setPassword(userDao.getPassword().trim());
    }
}
